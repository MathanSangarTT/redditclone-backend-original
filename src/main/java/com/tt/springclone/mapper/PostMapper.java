package com.tt.springclone.mapper;

import com.github.marlonlom.utilities.timeago.TimeAgo;
import com.tt.springclone.dto.NewPost;
import com.tt.springclone.dto.PostResponse;
import com.tt.springclone.model.*;
import com.tt.springclone.repo.CommentRepo;
import com.tt.springclone.repo.VoteRepo;
import com.tt.springclone.service.AuthService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.Optional;

import static com.tt.springclone.model.VoteType.DOWNVOTE;
import static com.tt.springclone.model.VoteType.UPVOTE;

@Mapper(componentModel = "spring")
public abstract class PostMapper {
    @Autowired
    AuthService authService;
    @Autowired
    CommentRepo commentRepo;
    @Autowired
    VoteRepo voteRepo;
    @Autowired
    Vote vote;

    @Mapping(target = "createdDate", expression = "java(java.time.Instant.now())")
    @Mapping(target = "description", source = "newPost.description")
    @Mapping(target = "postName", source = "newPost.postName")
    @Mapping(target = "voteCount", constant = "0")
    @Mapping(target = "subreddit" , source = "subreddit")
    @Mapping(target = "user" , source = "user")
    public abstract Post mapToModel(NewPost newPost, SubReddit subreddit, User user);

    @Mapping(target = "userName", source = "user.userName")
    @Mapping(target = "subredditName", source = "subreddit.name")
    @Mapping(target = "description" , source = "post.description")
    @Mapping(target = "commentCount", expression = "java(commentCount(post))")
    @Mapping(target = "duration", expression = "java(getDuration(post))")
    @Mapping(target = "upVote", expression = "java(isPostUpVoted(post))")
    @Mapping(target = "downVote", expression = "java(isPostDownVoted(post))")
    public abstract PostResponse mapToDto(Post post);

    Integer commentCount(Post post) {
        return commentRepo.findByPost(post).size();
    }

    String getDuration(Post post) {
        return TimeAgo.using(post.getCreatedDate().toEpochMilli());
    }

    boolean isPostUpVoted(Post post) {
        return checkVoteType(post, UPVOTE);
    }

    boolean isPostDownVoted(Post post) {
        return checkVoteType(post, DOWNVOTE);
    }

    private boolean checkVoteType(Post post, VoteType voteType) {
        if (authService.isLoggedIn()) {
            Optional<Vote> voteForPostByUser =
                    voteRepo.findTopByPostAndUserOrderByVoteIdDesc(post,
                            authService.getCurrentUser());
            System.out.println("......................................................."+voteForPostByUser);
            return voteForPostByUser.filter(vote -> vote.getVoteType().equals(voteType))
                    .isPresent();
        }
        return false;
    }
}
