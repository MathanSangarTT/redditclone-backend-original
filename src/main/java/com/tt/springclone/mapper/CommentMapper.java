package com.tt.springclone.mapper;


import com.tt.springclone.dto.NewComment;
import com.tt.springclone.model.Comment;
import com.tt.springclone.model.Post;
import com.tt.springclone.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface CommentMapper {
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "text", source = "newComment.text")
    @Mapping(target = "createdDate", expression = "java(java.time.Instant.now())")
    @Mapping(target = "post", source = "post")
    @Mapping(target = "user", source = "user")
    Comment map(NewComment newComment, Post post, User user);

    @Mapping(target = "postId", expression = "java(comment.getPost().getPostId())")
    @Mapping(target = "userName", expression = "java(comment.getUser().getUserName())")
    NewComment mapToDto(Comment comment);
}