package com.tt.springclone.security;

import com.tt.springclone.exception.SpringRedditException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.CertificateException;
import java.time.Instant;
import java.util.Date;

import static java.util.Date.from;

@Component
public class JwtCreator {

    private KeyStore keyStore;

    @Value("${jwt.expiration.time}")
    private Long jwtExpirationTime;


    @PostConstruct
    public void init() throws KeyStoreException {

        keyStore = KeyStore.getInstance("jks");
        InputStream resourceAsStream = getClass().getResourceAsStream("/keystore.jks");
        try {
            keyStore.load(resourceAsStream, "secret".toCharArray());
        } catch (IOException | NoSuchAlgorithmException | CertificateException e) {
            throw new SpringRedditException("error occured in keystore");
        }

    }

    public String generateToken(Authentication authentication) {
        User principal = (User) authentication.getPrincipal();
        return Jwts.builder().setSubject(principal.getUsername()).signWith(getPrivateKey())
                .setExpiration(from(
                Instant.now().plusMillis(jwtExpirationTime))).compact();
    }


    private PrivateKey getPrivateKey() {
        try {
            return (PrivateKey) keyStore.getKey("springblog", "secret".toCharArray());
        } catch (KeyStoreException | NoSuchAlgorithmException | UnrecoverableKeyException e) {
            throw new SpringRedditException("error occured in keystore");
        }
    }
    public Long getJwtExpirationTime()
    {
        return jwtExpirationTime;
    }

    public boolean validateToken(String token){
       Jwts.parser().setSigningKey(getPublicKey()).parseClaimsJws(token);
        return true;
    }

    private  PublicKey getPublicKey(){
        try {
            return  keyStore.getCertificate("springblog").getPublicKey();
        } catch (KeyStoreException e) {
            throw new SpringRedditException("error occured in keystore");
        }
    }

    public String getUserNameFromToken(String Token)
    {
        Claims claim = Jwts.parser().setSigningKey(getPublicKey()).parseClaimsJws(Token).getBody();

        return claim.getSubject();
    }
    public String generateTokenWithUserName(String username) {
        return Jwts.builder()
                .setSubject(username)
                .setIssuedAt(from(Instant.now()))
                .signWith(getPrivateKey())
                .setExpiration(from(Instant.now().plusMillis(jwtExpirationTime)))
                .compact();
    }



}
