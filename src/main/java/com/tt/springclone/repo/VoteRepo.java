package com.tt.springclone.repo;


import com.tt.springclone.model.Post;
import com.tt.springclone.model.User;
import com.tt.springclone.model.Vote;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface VoteRepo extends JpaRepository<Vote,Long> {
    Optional<Vote> findTopByPostAndUserOrderByVoteIdDesc(Post post, User currentUser);
}
