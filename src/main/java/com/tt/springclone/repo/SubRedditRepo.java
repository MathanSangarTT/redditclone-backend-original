package com.tt.springclone.repo;


import com.tt.springclone.model.SubReddit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SubRedditRepo extends JpaRepository<SubReddit,Long> {
    Optional<SubReddit> findByName(String subredditName);
    Optional<SubReddit> findById(Long Id);
}
