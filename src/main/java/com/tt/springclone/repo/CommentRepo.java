package com.tt.springclone.repo;


import com.tt.springclone.model.Comment;
import com.tt.springclone.model.Post;
import com.tt.springclone.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Arrays;
import java.util.List;

public interface CommentRepo extends JpaRepository<Comment,Long> {
    List<Comment> findByPost(Post post);

    List<Comment> findAllByUser(User user);
}
