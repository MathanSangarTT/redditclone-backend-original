package com.tt.springclone.repo;


import com.tt.springclone.model.Post;
import com.tt.springclone.model.SubReddit;
import com.tt.springclone.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PostRepo extends JpaRepository<Post,Long> {

    List<Post> findAllBySubreddit(SubReddit subreddit);
    List<Post> findByUser(User user);
}
