package com.tt.springclone.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;

import static javax.persistence.FetchType.LAZY;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long postId;
    private String postName;
    private String description;
    private Integer voteCount = 0;;
    private Instant createdDate;
    private String url;
    @ManyToOne(fetch = LAZY)
    @JoinColumn(name="userId",referencedColumnName ="userId")
    private User user;
    @ManyToOne(fetch = LAZY)
    @JoinColumn(name="id",referencedColumnName ="id")
    private SubReddit subreddit;
}
