package com.tt.springclone.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import javax.persistence.*;

import static javax.persistence.FetchType.LAZY;

@Entity
@Data
@Builder
@Component
@AllArgsConstructor
@NoArgsConstructor
public class Vote {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long voteId;
    private VoteType voteType;
@ManyToOne(fetch = LAZY)
@JoinColumn(name = "postId" ,referencedColumnName = "postId")
    private Post post;
@ManyToOne(fetch = LAZY)
@JoinColumn(name = "userId" ,referencedColumnName = "userId")
    private User user;
}
