package com.tt.springclone.model;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Data
public  class User {
@Id
@GeneratedValue(strategy= GenerationType.IDENTITY)
   private Long userId;
    private String userName;
    private String password;
    private String email;
    @CreationTimestamp
    @Column(name = "createdDate")
    private Instant createdDate;
}
