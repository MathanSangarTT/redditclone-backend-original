package com.tt.springclone.dto;

import lombok.Data;

@Data
public class NewUser {
    private String userName;
    private String emailId;
    private String password;
}
