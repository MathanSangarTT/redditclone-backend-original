package com.tt.springclone.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class NewPost {
    private  Long postID;
    private  String subredditName;
    private  String url;
    private String description;
    private String postName;
}
