package com.tt.springclone.service;

import com.tt.springclone.dto.AuthenticationResponse;
import com.tt.springclone.dto.LoginUser;
import com.tt.springclone.dto.NewUser;
import com.tt.springclone.dto.RefreshTokenRequest;
import com.tt.springclone.model.RefreshToken;
import com.tt.springclone.model.User;
import com.tt.springclone.repo.UserRepo;
import com.tt.springclone.security.JwtCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;

@Service
public class AuthService {

    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    UserRepo userRepo;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    JwtCreator jwtCreator;
    @Autowired
    RefreshTokenService refreshTokenService;
    @Transactional
    public void signUp(NewUser newuser)
    {
        User user = new User();
        user.setUserName(newuser.getUserName());
        user.setEmail(newuser.getEmailId());
        user.setPassword(passwordEncoder.encode(newuser.getPassword()));
        user.setCreatedDate(Instant.now());
        userRepo.save(user);
    }
    public AuthenticationResponse login(LoginUser loginUser)
    {

     Authentication authenticate =  authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginUser.getUsername(),loginUser.getPassword()));

     SecurityContextHolder.getContext().setAuthentication(authenticate);

        String token = jwtCreator.generateToken(authenticate);

return new AuthenticationResponse().builder().username(loginUser.getUsername()).token(token).refreshToken(refreshTokenService.generateRefreshToken().getToken()).expireTime(Instant.now().plusMillis(jwtCreator.getJwtExpirationTime())).build();
    }

    @Transactional
    public User getCurrentUser() {
        org.springframework.security.core.userdetails.User principal = (org.springframework.security.core.userdetails.User) SecurityContextHolder.
                getContext().getAuthentication().getPrincipal();

        return userRepo.findByUserName(principal.getUsername())
                .orElseThrow(() -> new UsernameNotFoundException("User name not found - " + principal.getUsername()));
    }
    public boolean isLoggedIn() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return !(authentication instanceof AnonymousAuthenticationToken) && authentication.isAuthenticated();
    }
    public AuthenticationResponse refreshToken(RefreshTokenRequest refreshTokenRequest) {
        refreshTokenService.validateRefreshToken(refreshTokenRequest.getRefreshToken());
        String token = jwtCreator.generateTokenWithUserName(refreshTokenRequest.getUsername());


        return AuthenticationResponse.builder()
                .token(token)
                .refreshToken(refreshTokenRequest.getRefreshToken())
                .expireTime(Instant.now().plusMillis(jwtCreator.getJwtExpirationTime()))
                .username(refreshTokenRequest.getUsername())
                .build();
    }
}
