package com.tt.springclone.service;

import com.tt.springclone.dto.NewPost;
import com.tt.springclone.dto.PostResponse;
import com.tt.springclone.exception.PostNotFoundException;
import com.tt.springclone.exception.SubredditNotFoundException;
import com.tt.springclone.mapper.PostMapper;
import com.tt.springclone.model.Post;
import com.tt.springclone.model.SubReddit;
import com.tt.springclone.model.User;
import com.tt.springclone.repo.PostRepo;
import com.tt.springclone.repo.SubRedditRepo;
import com.tt.springclone.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.util.stream.Collectors.toList;


@Service
public class PostService {

    @Autowired
    PostRepo postRepo;
    @Autowired
    SubRedditRepo subRedditRepo;
    @Autowired
    AuthService authService;
    @Autowired
    PostMapper postMapper;
    @Autowired
    UserRepo userRepo;
    @Transactional
    public Post save(NewPost newPost)
    {
        System.out.println(newPost.getSubredditName());

        SubReddit subReddit =  subRedditRepo.findByName(newPost.getSubredditName()).orElseThrow(()->new SubredditNotFoundException(newPost.getSubredditName()));
        User user = authService.getCurrentUser();
        System.out.println("post saving ....");
       return postRepo.save(postMapper.mapToModel(newPost,subReddit,user));
    }
    @Transactional(readOnly = true)
    public PostResponse getPost(Long id) {
        Post post = postRepo.findById(id)
                .orElseThrow(() -> new PostNotFoundException(id.toString()));
        return postMapper.mapToDto(post);
    }

    @Transactional(readOnly = true)
    public List<PostResponse> getAllPosts() {
        return postRepo.findAll()
                .stream()
                .map(postMapper::mapToDto)
                .collect(toList());
    }

    @Transactional(readOnly = true)
    public List<PostResponse> getPostsBySubreddit(Long subredditId) {
        System.out.println("subreddit id"+subredditId);
        SubReddit subreddit = subRedditRepo
                .findById(subredditId)
                .orElseThrow(() -> new SubredditNotFoundException(subredditId.toString()));

        System.out.println("get post by subreddit "+subreddit);
        List<Post> posts = postRepo.findAllBySubreddit(subreddit);
        return posts.stream().map(postMapper::mapToDto).collect(toList());
    }

    @Transactional(readOnly = true)
    public List<PostResponse> getPostsByUsername(String username) {
        User user = userRepo.findByUserName(username)
                .orElseThrow(() -> new UsernameNotFoundException(username));
        return postRepo.findByUser(user)
                .stream()
                .map(postMapper::mapToDto)
                .collect(toList());
    }

}
