package com.tt.springclone.service;

import com.tt.springclone.dto.SubredditDto;
import com.tt.springclone.exception.SpringRedditException;
import com.tt.springclone.mapper.SubredditMapper;
import com.tt.springclone.model.SubReddit;
import com.tt.springclone.repo.SubRedditRepo;
import org.hibernate.tool.hbm2ddl.SchemaUpdate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Service
public class SubredditService {

    @Autowired
    SubRedditRepo subRedditRepo;
    @Autowired
    SubredditMapper subredditMapper;
@Transactional
    public SubredditDto saveSubredditData(SubredditDto subredditDto)
    {
       SubReddit subReddit =  subRedditRepo.save(subredditMapper.mapToModel(subredditDto));
       subredditDto.setId(subReddit.getId());
       return subredditDto;
    }
    @Transactional
    public List<SubredditDto> getAll(){
    return subRedditRepo.findAll().stream().map(subredditMapper::mapToDto).collect(toList());
    }
@Transactional
    public SubredditDto getSubreddit(Long id) {
        SubReddit subreddit = subRedditRepo.findById(id)
                .orElseThrow(() -> new SpringRedditException("No subreddit found with ID - " + id));
        return subredditMapper.mapToDto(subreddit);
    }

}
