package com.tt.springclone.service;

import com.tt.springclone.dto.NewComment;
import com.tt.springclone.exception.PostNotFoundException;
import com.tt.springclone.mapper.CommentMapper;
import com.tt.springclone.model.Post;
import com.tt.springclone.model.User;
import com.tt.springclone.repo.CommentRepo;
import com.tt.springclone.repo.PostRepo;
import com.tt.springclone.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;
@Service
public class CommentService {
@Autowired
    PostRepo postRepo;
@Autowired
    CommentRepo commentRepo;
@Autowired
CommentMapper commentMapper;
@Autowired
    UserRepo userRepo;
@Autowired
AuthService authService;
    public void save(NewComment newComment)
    {

        Post post = postRepo.findById(newComment.getPostId()).orElseThrow(()->new PostNotFoundException(newComment.getId().toString()));


        System.out.println("new comment1111111111..............."+commentMapper.map(newComment,post,authService.getCurrentUser()));
        commentRepo.save(commentMapper.map(newComment,post,authService.getCurrentUser()));

    }

    public List<NewComment> getAllCommentsForPost(Long postId) {
        Post post = postRepo.findById(postId).orElseThrow(() -> new PostNotFoundException(postId.toString()));
        return commentRepo.findByPost(post)
                .stream()
                .map(commentMapper::mapToDto).collect(toList());
    }

    public List<NewComment> getAllCommentsForUser(String userName) {
        User user = userRepo.findByUserName(userName)
                .orElseThrow(() -> new UsernameNotFoundException(userName));
        return commentRepo.findAllByUser(user)
                .stream()
                .map(commentMapper::mapToDto)
                .collect(toList());
    }
    public void deleteComment(long id)
    {
        commentRepo.deleteById(id);
    }

}
