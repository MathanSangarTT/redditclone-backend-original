package com.tt.springclone.controller;


import com.tt.springclone.dto.SubredditDto;
import com.tt.springclone.model.SubReddit;
import com.tt.springclone.service.SubredditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/subreddit")
public class SubredditController {
    @Autowired
    SubredditService subredditService;
@PostMapping
    public ResponseEntity<SubredditDto> createSubreddit(@RequestBody SubredditDto subredditDto)
{
    System.out.println("creating subreddit......");
     return ResponseEntity.status(HttpStatus.CREATED).body(subredditService.saveSubredditData(subredditDto));
}
@GetMapping
    public ResponseEntity<List<SubredditDto>> getAllSubredditData()
{
    return  ResponseEntity.status(HttpStatus.OK).body(subredditService.getAll());
}
@GetMapping("/{id}")
    public ResponseEntity<SubredditDto> getSubredditBYId(@PathVariable Long id)
{
    return ResponseEntity.status(HttpStatus.OK).body(subredditService.getSubreddit(id));
}

}
