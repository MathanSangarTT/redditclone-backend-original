package com.tt.springclone.controller;

import com.tt.springclone.dto.AuthenticationResponse;
import com.tt.springclone.dto.LoginUser;
import com.tt.springclone.dto.NewUser;
import com.tt.springclone.dto.RefreshTokenRequest;
import com.tt.springclone.model.RefreshToken;
import com.tt.springclone.model.User;
import com.tt.springclone.repo.UserRepo;
import com.tt.springclone.service.AuthService;
import com.tt.springclone.service.RefreshTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/auth")
public class AuthenticationController {
    @Autowired
    AuthService authService;
    @Autowired
    RefreshTokenService refreshTokenService;
   @PostMapping("/signUp")
    public ResponseEntity  signUp(@RequestBody NewUser user)
   {
       authService.signUp(user);
    return new ResponseEntity<>("USER REG SUCCESS", OK);
   }
   @PostMapping("/login")
   public AuthenticationResponse login(@RequestBody LoginUser loginUser)
   {
           return authService.login(loginUser);
   }

    @PostMapping("/refresh/token")
    public AuthenticationResponse refreshTokens(@RequestBody RefreshTokenRequest refreshTokenRequest) {
        return authService.refreshToken(refreshTokenRequest);
    }
    @PostMapping("/logout")
    public ResponseEntity<String> logout(@RequestBody RefreshTokenRequest refreshTokenRequest) {
        refreshTokenService.deleteRefreshToken(refreshTokenRequest.getRefreshToken());
        return ResponseEntity.status(OK).body("Refresh Token Deleted Successfully!!");
    }
}