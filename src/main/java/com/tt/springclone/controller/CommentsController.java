package com.tt.springclone.controller;

import com.tt.springclone.dto.NewComment;
import com.tt.springclone.dto.NewUser;
import com.tt.springclone.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import java.util.List;

@RestController
@RequestMapping("/api/comments")
public class CommentsController {
    @Autowired
    CommentService commentService;
    @PostMapping
    public ResponseEntity  addComment(@RequestBody NewComment newComment)
    {
          commentService.save(newComment);
        return new ResponseEntity<>(CREATED);
    }
    @GetMapping("/by-post/{postId}")
    public ResponseEntity<List<NewComment>> getAllCommentsForPost(@PathVariable Long postId) {
        return ResponseEntity.status(OK)
                .body(commentService.getAllCommentsForPost(postId));
    }

    @GetMapping("/by-user/{userName}")
    public ResponseEntity<List<NewComment>> getAllCommentsForUser(@PathVariable String userName){
        return ResponseEntity.status(OK)
                .body(commentService.getAllCommentsForUser(userName));
    }
    @GetMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable long id)
    {

        return new ResponseEntity<>(OK);
    }
}
