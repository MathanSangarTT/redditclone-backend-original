package com.tt.springclone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
public class StartApplication {

	public static void main(String[] args) {

		System.out.println("started........");
		SpringApplication.run(StartApplication.class, args);
	}

}
